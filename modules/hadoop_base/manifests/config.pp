class hadoop_base::config inherits hadoop_base {

  require user_base::hadoop
  require dir_base::src
  require dir_base::hadoop
  require hadoop_base::install

  file { $config_core_site:
    ensure  => file,
    # owner   => elasticsearch,
    # group   => root,
    mode    => 0644,
    content => template($config_template_core_site),
  }

  file { $config_hadoop_env:
    ensure  => file,
    # owner   => elasticsearch,
    # group   => root,
    mode    => 0644,
    content => template($config_template_hadoop_env),
  }

  file { $config_hdfs_site:
    ensure  => file,
    # owner   => elasticsearch,
    # group   => root,
    mode    => 0644,
    content => template($config_template_hdfs_site),
  }

  file { $config_slaves:
    ensure     => file,
    # owner      => elasticsearch,
    # group      => root,
    mode       => 0644,
    content    => template($config_template_slaves),
  }

  file { $config_mapred_site:
    ensure     => file,
    owner      => hadoop,
    group      => hadoop,
    mode       => 0644,
    content    => template($config_template_mapred_site),
  }

  file { $config_yarn_site:
    ensure     => file,
    # owner      => elasticsearch,
    # group      => root,
    mode       => 0644,
    content    => template($config_template_yarn_site),
  }

  file { "/home/hadoop/.bashrc":
    ensure     => file,
    # owner      => 'hadoop',
    # group      => 'hadoop',
    # mode       => 0644,
    content	   =>	template ('hadoop_base/bashrc.erb'),
    require    => User['hadoop'],
  }

  # file { $config_supervisord:
  #   ensure  => file,
  #   owner   => root,
  #   group   => root,
  #   mode    => '0644',
  #   require => Package['supervisor'],
  #   content => template($config_template_supervisor),
  # }

}
