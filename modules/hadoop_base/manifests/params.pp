class hadoop_base::params {

    $service_name        = 'hadoop'
    $service_fullname    = 'hadoop'
    $service_module      = 'hadoop_base'
    $daemon_default      = 'namenode'
    $version             = '2.7.2'
    $extension           = 'tar.gz'
    $package_dir         = '/opt/src'

    $config_template_core_site     = "$service_module/core-site.xml.erb"
    $config_template_hadoop_env    = "$service_module/hadoop-env.sh.erb"
    $config_template_hdfs_site     = "$service_module/hdfs-site.xml.erb"
    $config_template_slaves        = "$service_module/slaves.erb"
    $config_template_mapred_site   = "$service_module/mapred-site.xml.erb"
    $config_template_yarn_site     = "$service_module/yarn-site.xml.erb"

    # http://www-eu.apache.org/dist/hadoop/common/hadoop-2.7.2/hadoop-2.7.2.tar.gz
    $path_url_us         = 'http://www-eu.apache.org/dist/hadoop/common'
    # http://mirrors.viethosting.vn/apache/hadoop/common/hadoop-2.7.2/hadoop-2.7.2.tar.gz
    # $path_url_vn         = 'http://mirrors.viethosting.vn/apache/hadoop/common'
    # http://mirror.downloadvn.com/apache/hadoop/common/hadoop-2.7.3/hadoop-2.7.3.tar.gz
    $path_url_vn         = 'http://mirror.downloadvn.com/apache/hadoop/common'

    $service_restart     = true

}
