class hadoop_base (

  $service_name        = $hadoop_base::params::service_name,
  $service_fullname    = $hadoop_base::params::service_fullname,
  $service_module      = $hadoop_base::params::service_module,
  $daemon_default      = $hadoop_base::params::daemon_default,
  $version             = $hadoop_base::params::version,
  $extension           = $hadoop_base::params::extension,
  $package_dir         = $hadoop_base::params::package_dir,

  $config_template_core_site     = $hadoop_base::params::config_template_core_site,
  $config_template_hadoop_env    = $hadoop_base::params::config_template_hadoop_env,
  $config_template_hdfs_site     = $hadoop_base::params::config_template_hdfs_site,
  $config_template_slaves        = $hadoop_base::params::config_template_slaves,
  $config_template_mapred_site   = $hadoop_base::params::config_template_mapred_site,
  $config_template_yarn_site     = $hadoop_base::params::config_template_yarn_site,

  # http://www-eu.apache.org/dist/hadoop/common/hadoop-2.7.2/hadoop-2.7.2.tar.gz
  $path_url_us         = $hadoop_base::params::path_url_us,
  # http://mirrors.viethosting.vn/apache/hadoop/common/hadoop-2.7.2/hadoop-2.7.2.tar.gz
  $path_url_vn         = $hadoop_base::params::path_url_vn,

  $service_restart     = $hadoop_base::params::service_restart,

  ) inherits hadoop_base::params {

    $package_name        = "$service_fullname-$version"
    $install_dir         = "$package_dir/$package_name"
    $config_dir          = "$install_dir/etc/hadoop"
    $config_core_site    = "$config_dir/core-site.xml"
    $config_hadoop_env   = "$config_dir/hadoop-env.sh"
    $config_hdfs_site    = "$config_dir/hdfs-site.xml"
    $config_slaves       = "$config_dir/slaves"
    $config_mapred_site  = "$config_dir/mapred-site.xml"
    $config_yarn_site    = "$config_dir/yarn-site.xml"
    $daemon              = $daemon_default

    include hadoop_base::config
    include hadoop_base::service

}
