class hadoop_base::install inherits hadoop_base {

    if $hostname == 'c1' {
      # $mirror = $mirror_vn
      # $mirror_vn           = "$path_url_vn/$package_name/$package_name.$extension"
      $mirror  = "$path_url_vn/$package_name/$package_name.$extension"
    }else{
      # $mirror = $mirror_us
      # $mirror_us           = "$path_url_us/$package_name/$package_name.$extension"
      $mirror  = "$path_url_us/$package_name/$package_name.$extension"
    }

    exec { 'tar_package_hadoop':
      command => "tar -xzf $install_dir.$extension",
      cwd     => "/opt/src",
      path    => ["/bin"],
      creates => "$install_dir",
      require => Exec['wget_package_hadoop'],
    }

    exec { 'wget_package_hadoop':
      command   => "wget $mirror -O $install_dir.$extension",
      path      => ["/sbin","/bin","/usr/bin", "/usr/sbin"],
      creates   => "$install_dir.$extension",
    }

    file { $install_dir:
      ensure => directory,
      recurse => true,
      # owner => "root",
      # group => "root",
      require => Exec['tar_package_hadoop'],
    }

}
