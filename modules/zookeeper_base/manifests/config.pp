class zookeeper_base::config inherits zookeeper_base {
    require dir_base::src
    require dir_base::zookeeper
    require zookeeper_base::install

    file { $config:
      ensure  => file,
      owner   => root,
      group   => root,
      mode    => '0644',
      content => template('zookeeper_base/zoo.cfg.erb'),
    }

    file{ "$dataDir/myid":
      ensure  => file,
      owner   => root,
      group   => root,
      mode    => '0644',
      content => template('zookeeper_base/myid.erb'),
    }

    file { $config_env:
      ensure  => present,
      content => "export ZK_HOME=$install_dir; PATH=\${PATH}:$install_dir/bin",
      require => Service[$service_name],
    }

    file { $config_supervisord:
      ensure  => file,
      owner   => root,
      group   => root,
      mode    => '0644',
      require => Package['supervisor'],
      content => template($config_template_supervisor),
    }

}
