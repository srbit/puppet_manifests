class zookeeper_base::params {
  $prefix              = 'a'
  $myid                = 1
  $service_name        = 'zookeeper'
  $service_module      = 'zookeeper_base'
  $version             = '3.4.8'
  $extension           = 'tar.gz'
  $package_dir         = '/opt/src'
  $package_name        = "$service_name-$version"
  $install_dir         = "$package_dir/$package_name"
  $config_dir          = "$install_dir/conf"
  $config_env          = '/etc/profile.d/zookeeper.sh'
  $config_template     = "$service_module/zoo.cfg.erb"
  $config              = "$config_dir/zoo.cfg"
  $mirror_us           = "http://www-eu.apache.org/dist/$service_name/$package_name/$package_name.$extension"
  $mirror_vn           = "http://mirrors.viethosting.vn/apache/$service_name/$package_name/$package_name.$extension"

  $tickTime            = 2000
  $initLimit           = 10
  $syncLimit           = 5
  $dataDir             = "$package_dir/logs_zookeeper/zookeeper"
  $dataLogDir          = "$package_dir/logs_zookeeper/logs"
  $clientPort          = 2181
  $leaderPort          = 3888
  $leaderElectionPort  = 2888
  $maxClientCnxns      = 1000
  $autopurge_snapRetainCount  = 3
  $autopurge_purgeInterval    = 1
  $pid_dir             = '/var/run'
  $pid_file            = undef
  $service_restart     = true

  $zoo_dir             = '/usr/lib/zookeeper'
  $zoo_main            = 'org.apache.zookeeper.server.quorum.QuorumPeerMain'
  $log4j_prop          = 'INFO,ROLLINGFILE'

  $config_supervisord         = '/etc/supervisord.conf'
  $config_template_supervisor = "$service_module/supervisord.conf.erb"

}
