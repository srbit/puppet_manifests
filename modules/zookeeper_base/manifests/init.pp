class zookeeper_base (
  $prefix              = $zookeeper_base::params::prefix,
  $myid                = $zookeeper_base::params::myid,
  $service_name        = $zookeeper_base::params::service_name,
  $version             = $zookeeper_base::params::version,
  $extension           = $zookeeper_base::params::extension,
  $package_dir         = $zookeeper_base::params::package_dir,
  $package_name        = $zookeeper_base::params::package_name,
  $install_dir         = $zookeeper_base::params::install_dir,
  $mirror_us           = $zookeeper_base::params::mirror_us,
  $mirror_vn           = $zookeeper_base::params::mirror_vn,

  $tickTime            = $zookeeper_base::params::tickTime,
  $initLimit           = $zookeeper_base::params::initLimit,
  $syncLimit           = $zookeeper_base::params::syncLimit,
  $dataDir             = $zookeeper_base::params::dataDir,
  $dataLogDir          = $zookeeper_base::params::dataLogDir,
  $clientPort          = $zookeeper_base::params::clientPort,
  $maxClientCnxns      = $zookeeper_base::params::maxClientCnxns,
  $autopurge_snapRetainCount  = $zookeeper_base::params::autopurge_snapRetainCount,
  $autopurge_purgeInterval    = $zookeeper_base::params::autopurge_purgeInterval,
  $pid_dir             = $zookeeper_base::params::pid_dir,
  $pid_file            = $zookeeper_base::params::pid_file,

  $config              = $zookeeper_base::params::config,
  $config_env          = $zookeeper_base::params::config_env,
  $config_template     = $zookeeper_base::params::config_template,

  $zoo_dir             = $zookeeper_base::params::zoo_dir,
  $zoo_main            = $zookeeper_base::params::zoo_main,

  $config_supervisord         = $zookeeper_base::params::config_supervisord,
  $config_template_supervisor = $zookeeper_base::params::config_template_supervisor

  ) inherits zookeeper_base::params {

    if $pid_file {
      $pid_path        = $pid_file
    } else {
      $pid_path        = "${pid_dir}/zookeeper.pid"
    }

    include zookeeper_base::config
    include zookeeper_base::service

}
