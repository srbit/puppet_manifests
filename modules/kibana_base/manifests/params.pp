class kibana_base::params {

    $service_name        = 'kibana'
    $service_fullname    = 'kibana'
    $service_module      = 'kibana_base'
    $version             = '4.1.11'
    $os                  = 'linux'
    $arch_x64            = 'x64'
    $arch_x86_64         = 'x86_64'
    $extension           = 'tar.gz'
    $package_dir         = '/opt/src'
    $config_template     = "$service_module/kibana.yml.erb"

    # https://artifacts.elastic.co/downloads/kibana/kibana-5.1.1-linux-x86_64.tar.gz
    $path_url_5x = "https://artifacts.elastic.co/downloads/$service_name"
    # https://download.elastic.co/kibana/kibana/kibana-4.6.3-linux-x86_64.tar.gz
    # https://download.elastic.co/kibana/kibana/kibana-4.1.11-linux-x64.tar.gz
    $path_url_4x = "https://download.elastic.co/$service_name/$service_name"

    $service_restart     = true

    # Config Kibana 4.1.x
    $port         = 5601
    $host         = '0.0.0.0'
    $elasticsearch_url= "http://$hostname:9200"
    $elasticsearch_preserve_host   = 'true'
    $kibana_index    = '.kibana'
    $kibana_elasticsearch_username   = 'user'
    $kibana_elasticsearch_password   = 'pass'
    $kibana_elasticsearch_client_crt   = '/path/to/your/client.crt'
    $kibana_elasticsearch_client_key   = '/path/to/your/client.key'
    $ca                   = '/path/to/your/CA.pem'
    $default_app_id       = 'discover'
    $ping_timeout         = 1500
    $request_timeout      = 300000
    $shard_timeout        = 0
    $startup_timeout      = 5000
    $verify_ssl           = true
    $ssl_key_file         = '/path/to/your/server.key'
    $ssl_cert_file        = '/path/to/your/server.crt'
    $pid_file             = '/var/run/kibana.pid'
    $log_file             = './kibana.log'
    $xsrf_token           = ""

    # Config Kibana 4.6.x
    $server_port                  = 5601
    $server_host                  = '0.0.0.0'
    $server_basePath              = ""
    $server_maxPayloadBytes       = 1048576
    $elasticsearch_preserveHost   = true
    $kibana_defaultAppId          = 'discover'
    $elasticsearch_username       = 'user'
    $elasticsearch_password       = 'pass'
    $server_ssl_cert              = '/path/to/your/server.crt'
    $server_ssl_key               = '/path/to/your/server.key'
    $elasticsearch_ssl_cert       = '/path/to/your/client.crt'
    $elasticsearch_ssl_key        = '/path/to/your/client.key'
    $elasticsearch_ssl_ca         = '/path/to/your/CA.pem'
    $elasticsearch_ssl_verify     = true
    $elasticsearch_pingTimeout    = 1500
    $elasticsearch_requestTimeout = 30000
    $elasticsearch_customHeaders  = {}
    $elasticsearch_shardTimeout   = 0
    $elasticsearch_startupTimeout = 5000
    $logging_dest                 = stdout
    $logging_silent               = false
    $logging_quiet                = false
    $logging_verbose              = false

    # Config Kibana 5.x
    $server_name                            = 'your-hostname'
    $elasticsearch_requestHeadersWhitelist  = 'authorization'
    $ops_interval                           = 5000

}
