class kibana_base (
  $service_name        = $kibana_base::params::service_name,
  $service_fullname    = $kibana_base::params::service_fullname,
  $service_module      = $kibana_base::params::service_module,
  $version             = $kibana_base::params::version,
  $os                  = $kibana_base::params::os,
  $arch_x64            = $kibana_base::params::arch_x64,
  $arch_x86_64         = $kibana_base::params::arch_x86_64,
  $extension           = $kibana_base::params::extension,
  $package_dir         = $kibana_base::params::package_dir,
  $config_template     = $kibana_base::params::config_template,

  # https://artifacts.elastic.co/downloads/kibana/kibana-5.1.1-linux-x86_64.tar.gz
  $path_url_5x         = $kibana_base::params::path_url_5x,
  # https://download.elastic.co/kibana/kibana/kibana-4.6.3-linux-x86_64.tar.gz
  # https://download.elastic.co/kibana/kibana/kibana-4.1.11-linux-x64.tar.gz
  $path_url_4x         = $kibana_base::params::path_url_4x,

  $service_restart     = $kibana_base::params::service_restart,

  # Config Kibana 4.1.x
  $port                = $kibana_base::params::port,
  $host                = $kibana_base::params::host,
  $elasticsearch_url   = $kibana_base::params::elasticsearch_url,
  $elasticsearch_preserve_host       = $kibana_base::params::elasticsearch_preserve_host,
  $kibana_index                      = $kibana_base::params::kibana_index,
  $kibana_elasticsearch_username     = $kibana_base::params::kibana_elasticsearch_username,
  $kibana_elasticsearch_password     = $kibana_base::params::kibana_elasticsearch_password,
  $kibana_elasticsearch_client_crt   = $kibana_base::params::kibana_elasticsearch_client_crt,
  $kibana_elasticsearch_client_key   = $kibana_base::params::kibana_elasticsearch_client_key,
  $ca                   = $kibana_base::params::ca,
  $default_app_id       = $kibana_base::params::default_app_id,
  $ping_timeout         = $kibana_base::params::ping_timeout,
  $request_timeout      = $kibana_base::params::request_timeout,
  $shard_timeout        = $kibana_base::params::shard_timeout,
  $startup_timeout      = $kibana_base::params::startup_timeout,
  $verify_ssl           = $kibana_base::params::verify_ssl,
  $ssl_key_file         = $kibana_base::params::ssl_key_file,
  $ssl_cert_file        = $kibana_base::params::ssl_cert_file,
  $pid_file             = $kibana_base::params::pid_file,
  $log_file             = $kibana_base::params::log_file,
  $xsrf_token           = $kibana_base::params::xsrf_token,

  # Config Kibana 4.6.x
  $server_port                  = $kibana_base::params::server_port,
  $server_host                  = $kibana_base::params::server_host,
  $server_basePath              = $kibana_base::params::server_basePath,
  $server_maxPayloadBytes       = $kibana_base::params::server_maxPayloadBytes,
  $elasticsearch_preserveHost   = $kibana_base::params::elasticsearch_preserveHost,
  $kibana_defaultAppId          = $kibana_base::params::kibana_defaultAppId,
  $elasticsearch_username       = $kibana_base::params::elasticsearch_username,
  $elasticsearch_password       = $kibana_base::params::elasticsearch_password,
  $server_ssl_cert              = $kibana_base::params::server_ssl_cert,
  $server_ssl_key               = $kibana_base::params::server_ssl_key,
  $elasticsearch_ssl_cert       = $kibana_base::params::elasticsearch_ssl_cert,
  $elasticsearch_ssl_key        = $kibana_base::params::elasticsearch_ssl_key,
  $elasticsearch_ssl_ca         = $kibana_base::params::elasticsearch_ssl_ca,
  $elasticsearch_ssl_verify     = $kibana_base::params::elasticsearch_ssl_verify,
  $elasticsearch_pingTimeout    = $kibana_base::params::elasticsearch_pingTimeout,
  $elasticsearch_requestTimeout = $kibana_base::params::elasticsearch_requestTimeout,
  $elasticsearch_customHeaders  = $kibana_base::params::elasticsearch_customHeaders,
  $elasticsearch_shardTimeout   = $kibana_base::params::elasticsearch_shardTimeout,
  $elasticsearch_startupTimeout = $kibana_base::params::elasticsearch_customHeaders,
  $logging_dest                 = $kibana_base::params::logging_dest,
  $logging_silent               = $kibana_base::params::logging_silent,
  $logging_quiet                = $kibana_base::params::logging_quiet,
  $logging_verbose              = $kibana_base::params::logging_verbose,

  # Config Kibana 5.x
  $server_name                            = $kibana_base::params::server_name,
  $elasticsearch_requestHeadersWhitelist  = $kibana_base::params::elasticsearch_requestHeadersWhitelist,
  $ops_interval                           = $kibana_base::params::ops_interval,
  ) inherits kibana_base::params {

    if($version=='4.1.11'){
      $package_name        = "$service_fullname-$version-$os-$arch_x64"
    } else {
      $package_name        = "$service_fullname-$version-$os-$arch_x86_64"
    }
    $install_dir         = "$package_dir/$package_name"
    $config_dir          = "$install_dir/config"
    $config              = "$config_dir/kibana.yml"

    include kibana_base::config
    include kibana_base::service

}
