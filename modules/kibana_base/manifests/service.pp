class kibana_base::service inherits kibana_base {

    file { "${service_name}.service":
      ensure  => file,
      path    => "/etc/init.d/${service_name}",
      owner   => elasticsearch,
      # group   => root,
      mode    => '0755',
      content => template('kibana_base/init.erb'),
      before  => Service[$service_name],
    }

    service { $service_name:
      ensure     => $service_ensure,
      enable     => true,
      hasstatus  => true,
      hasrestart => true,
    }

}
