# == Class: kibana_base::install
#
class kibana_base::install inherits kibana_base {

      if($version=='5.1.1'){
        $mirror = "$path_url_5x/$package_name.$extension"
      }
      if($version=='4.6.3'){
        $mirror = "$path_url_4x/$package_name.$extension"
      }
      if($version=='4.1.11'){
        $mirror = "$path_url_4x/$package_name.$extension"
      }

      exec { 'tar_package_kibana':
        command => "tar -xzf $install_dir.$extension",
        cwd     => "/opt/src",
        path    => ["/bin"],
        creates => "$install_dir",
        require => Exec['wget_package_kibana'],
      }

      exec { 'wget_package_kibana':
        command   => "wget $mirror -O $install_dir.$extension",
        path      => ["/sbin","/bin","/usr/bin", "/usr/sbin"],
        creates   => "$install_dir.$extension",
      }

      file { $install_dir:
        ensure => directory,
        recurse => true,
        # owner => "root",
        # group => "root",
        require => Exec['tar_package_kibana'],
      }

  }
