class jdk_base::install inherits jdk_base {

  require dir_base::java

  $mirror_jdk   = "$path_url_jdk/$version-$java_build/$java_hash/$package_name.$extension"
  $mirror_jce   = "$path_url_jce/$jce_version/$package_jce.$jce_extension"

# Load file jdk by using wget command

  exec { 'wget_package_jdk':
    cwd     => "/usr/java",
    creates => "$tmp_dir.$extension",
    command => "$wget_header $cookie \"$mirror_jdk\" -O $package_name.$extension",
    path    => ["/sbin","/bin","/usr/bin", "/usr/sbin"],
  }

  exec { 'wget_package_jce':
    cwd     => "/usr/java",
    creates => "$package_jce_dir.$jce_extension",
    command => "$wget_header $cookie \"$mirror_jce\" -O $package_jce.$jce_extension",
    path    => ["/sbin","/bin","/usr/bin", "/usr/sbin"],
  }

  exec { 'tar_package_jdk':
    command => "tar -xzf $package_name.$extension",
    cwd     => "/usr/java",
    path    => ["/bin"],
    creates => "$install_dir",
    require => Exec['wget_package_jdk'],
  }

  exec { 'install_java':
    command => "alternatives --install /usr/bin/java java /usr/java/$package_dir_tar/bin/java 2000",
    cwd     => "/usr/java",
    path    => ["/usr/sbin"],
    require => Exec['tar_package_jdk'],
  }

  exec { 'install_javac':
    command => "alternatives --install /usr/bin/javac javac /usr/java/$package_dir_tar/bin/javac 2000",
    cwd     => "/usr/java",
    path    => ["/usr/sbin"],
    require => Exec['tar_package_jdk'],
  }

  exec { 'install_jar':
    command => "alternatives --install /usr/bin/jar jar /usr/java/$package_dir_tar/bin/jar 2000",
    cwd     => "/usr/java",
    path    => ["/usr/sbin"],
    require => Exec['tar_package_jdk'],
  }

}
