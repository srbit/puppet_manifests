class jdk_base::config inherits jdk_base {

  # file { $config_env:
  #   ensure  => file,
  #   mode    => 0644,
  #   content => template($config_template_env),
  # }

  file { $config_env:
    ensure  => present,
    content => "export JAVA_HOME=$install_dir; PATH=\${PATH}:$install_dir/bin",
    require => Exec['install_java'],
  }

}
