class jdk_base (
  $service_name        = $jdk_base::params::service_name,
  $service_fullname    = $jdk_base::params::service_fullname,
  $service_module      = $jdk_base::params::service_module,
  $version             = $jdk_base::params::version,
  $version_tar         = $jdk_base::params::version_tar,
  $os                  = $jdk_base::params::os,
  $arch_x64            = $jdk_base::params::arch_x64,
  $extension           = $jdk_base::params::extension,
  $package_dir         = $jdk_base::params::package_dir,
  $config_env          = $jdk_base::params::config_env,
  $config_template_env = $jdk_base::params::config_template_env,
  $java_update         = $jdk_base::params::java_update,
  $java_update_        = $jdk_base::params::java_update_,
  $java_build          = $jdk_base::params::java_build,
  $java_hash           = $jdk_base::params::java_hash,

  $jce                 = $jdk_base::params::jce,
  $jce_version         = $jdk_base::params::jce_version,
  $jce_extension       = $jdk_base::params::jce_extension,

  # http://download.oracle.com/otn-pub/java/jdk/8u121-b13/e9e7ea248e2c4826b92b3f075a80e441/jdk-8u121-linux-x64.tar.gz
  # http://download.oracle.com/otn-pub/java/jdk/8u121-b13/jdk-8u121-linux-x64.tar.gz
  # http://download.oracle.com/otn-pub/java/jce/8/jce_policy-8.zip

  # http://download.oracle.com/otn/java/jdk/7u80-b15/jdk-7u80-linux-x64.tar.gz
  # http://download.oracle.com/otn-pub/java/jdk/7u80-b15/jdk-7u80-linux-x64.tar.gz
  # http://download.oracle.com/otn-pub/java/jce/7/UnlimitedJCEPolicyJDK7.zip

  $path_url_jdk        = $jdk_base::params::path_url_jdk,
  $path_url_jce        = $jdk_base::params::path_url_jce,

  $wget_header         = $jdk_base::params::wget_header,
  $cookie              = $jdk_base::params::cookie,

  ) inherits jdk_base::params {

    $package_name      = "$service_fullname-$version-$os-$arch_x64"
    $package_dir_tar   = "$service_name$version_tar$java_update_"
    $install_dir       = "$package_dir/$package_dir_tar"
    $tmp_dir           = "$package_dir/$package_name"

    $package_jce       = "$jce-$jce_version"
    $package_jce_dir   = "$package_dir/$package_jce"

    include jdk_base::install
    include jdk_base::config

}
