class jdk_base::params {
  $service_name        = 'jdk'
  $service_fullname    = 'jdk'
  $service_module      = 'jdk_base'
  $version             = '8u121'
  $version_tar         = '1.8.0'
  $os                  = 'linux'
  $arch_x64            = 'x64'
  $extension           = 'tar.gz'
  $package_dir         = '/usr/java'
  $config_env          = '/etc/profile.d/java.sh'
  $config_template_env = "$service_module/java.sh.erb"

  $java_update         = '121'
  $java_update_        = '_121'
  $java_build          = 'b13'
  $java_hash           = 'e9e7ea248e2c4826b92b3f075a80e441'

  $jce                 = 'jce_policy'
  $jce_version         = 8
  $jce_extension       = 'zip'

  # http://download.oracle.com/otn-pub/java/jdk/8u121-b13/e9e7ea248e2c4826b92b3f075a80e441/jdk-8u121-linux-x64.tar.gz
  # http://download.oracle.com/otn-pub/java/jdk/8u121-b13/jdk-8u121-linux-x64.tar.gz
  # http://download.oracle.com/otn-pub/java/jce/8/jce_policy-8.zip

  # http://download.oracle.com/otn/java/jdk/7u80-b15/jdk-7u80-linux-x64.tar.gz
  # http://download.oracle.com/otn-pub/java/jdk/7u80-b15/jdk-7u80-linux-x64.tar.gz
  # http://download.oracle.com/otn-pub/java/jce/7/UnlimitedJCEPolicyJDK7.zip

  $path_url_jdk        = "http://download.oracle.com/otn-pub/java/jdk"
  $path_url_jce        = "http://download.oracle.com/otn-pub/java/jce"

  $wget_header         = 'wget -c --no-cookies --no-check-certificate --header'
  $cookie              = "\"Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com; oraclelicense=accept-securebackup-cookie\""

}
