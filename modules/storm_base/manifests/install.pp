class storm_base::install inherits storm_base {

    if $hostname == 'c1' {
      $mirror = $mirror_vn
    }else{
      $mirror = $mirror_us
    }

    exec { "tar -xzf $install_dir.$extension":
      cwd     => "/opt/src",
      path    => ["/bin"],
      creates => "$install_dir",
      require => Exec['wget_package_storm'],
    }

    exec { 'wget_package_storm':
      command   => "wget $mirror -O $install_dir.$extension",
      path      => ["/sbin","/bin","/usr/bin", "/usr/sbin"],
      creates   => "$install_dir.$extension",
    }

}
