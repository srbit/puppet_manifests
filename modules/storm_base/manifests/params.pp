class storm_base::params {

    $service_name        = 'storm'
    $service_fullname    = 'apache-storm'
    $service_module      = 'storm_base'
    $version             = '0.9.6'
    $extension           = 'tar.gz'
    $package_dir         = '/opt/src'
    $package_name        = "$service_fullname-$version"
    $install_dir         = "$package_dir/$package_name"
    $config_dir          = "$install_dir/conf"
    $config_template     = "$service_module/storm.yaml.erb"
    $config              = "$config_dir/storm.yaml"
    $mirror_us           = "http://www-eu.apache.org/dist/$service_name/$package_name/$package_name.$extension"
    $mirror_vn           = "http://mirrors.viethosting.vn/apache/$service_name/$package_name/$package_name.$extension"

    $service_restart     = true
    $host_nimbus         = "anb"
    $worker_childopts    = "-Xmx1024m"

    $storm_zookeeper_connection_timeout = 300000
    $storm_zookeeper_session_timeout = 300000
    $storm_messaging_netty_server_worker_threads = 1
    $storm_messaging_netty_client_worker_threads = 1
    $storm_messaging_netty_buffer_size = 5242880
    $storm_messaging_netty_max_retries = 100
    $storm_messaging_netty_max_wait_ms = 1000
    $storm_messaging_netty_min_wait_ms = 100

    $config_supervisord         = '/etc/supervisord.conf'
    $config_template_supervisor = "$service_module/supervisord.conf.erb"

}
