class storm_base (

  $service_name        = $storm_base::params::service_name,
  $version             = $storm_base::params::version,
  $extension           = $storm_base::params::extension,
  $package_dir         = $storm_base::params::package_dir,
  $package_name        = $storm_base::params::package_name,
  $install_dir         = $storm_base::params::install_dir,
  $mirror_us           = $storm_base::params::mirror_us,
  $mirror_vn           = $storm_base::params::mirror_vn,
  $config_dir          = $storm_base::params::config_dir,
  $config_template     = $storm_base::params::config_template,
  $config              = $storm_base::params::config,

  $host_nimbus         = $storm_base::params::host_nimbus,
  $worker_childopts    = $storm_base::params::worker_childopts,

  $storm_zookeeper_connection_timeout         = $storm_base::params::storm_zookeeper_connection_timeout,
  $storm_zookeeper_session_timeout            = $storm_base::params::storm_zookeeper_session_timeout,
  $storm_messaging_netty_server_worker_threads  = $storm_base::params::storm_messaging_netty_server_worker_threads,
  $storm_messaging_netty_client_worker_threads  = $storm_base::params::storm_messaging_netty_client_worker_threads,
  $storm_messaging_netty_buffer_size          = $storm_base::params::storm_messaging_netty_buffer_size,
  $storm_messaging_netty_max_retries          = $storm_base::params::storm_messaging_netty_max_retries,
  $storm_messaging_netty_max_wait_ms          = $storm_base::params::storm_messaging_netty_max_wait_ms,
  $storm_messaging_netty_min_wait_ms          = $storm_base::params::storm_messaging_netty_min_wait_ms,

  $config_supervisord           = $storm_base::params::config_supervisord,
  $config_template_supervisor   = $storm_base::params::config_template_supervisor,

  ) inherits storm_base::params {

  include storm_base::config
  include storm_base::service

}
