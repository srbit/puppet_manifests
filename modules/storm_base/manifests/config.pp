class storm_base::config inherits storm_base {

    require dir_base::src
    # require dir_base::storm
    require storm_base::install

    file { $config:
      ensure  => file,
      owner   => root,
      group   => root,
      mode    => '0644',
      content => template($config_template),
    }

    file { $config_supervisord:
      ensure  => file,
      owner   => root,
      group   => root,
      mode    => '0644',
      require => Package['supervisor'],
      content => template($config_template_supervisor),
    }

}
