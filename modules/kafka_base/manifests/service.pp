class kafka_base::service inherits kafka_base {

  file { "${service_name}.service":
        ensure  => file,
        path    => "/etc/init.d/${service_name}",
        mode    => '0755',
        content => template('kafka_base/init.erb'),
        before  => Service[$service_name],
      }

    service { $service_name:
      ensure     => $service_ensure,
      enable     => true,
      hasstatus  => true,
      hasrestart => true,
    }
}
