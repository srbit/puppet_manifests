# == Class: kafka_base::config
class kafka_base::config inherits kafka_base {

  file { $config:
    ensure  => file,
    owner   => root,
    group   => root,
    mode    => '0644',
    content => template('kafka_base/server.properties.erb'),
  }

  file { $config_env:
    ensure  => file,
    # owner   => root,
    # group   => root,
    mode    => 0644,
    content => template($config_template_env),
  }

}
