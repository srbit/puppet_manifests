# == Class: kafka_base

class kafka_base (

$service_name        = $kafka_base::params::service_name,
$version             = $kafka_base::params::version,
$scala_version       = $kafka_base::params::scala_version,
$install_dir         = $kafka_base::params::install_dir,
$kafka_version       = $kafka_base::params::kafka_version,
$link_kafka_us       = $kafka_base::params::link_kafka_us,
$link_kafka_vn       = $kafka_base::params::link_kafka_vn,
$broker_id           = $kafka_base::params::broker_id,
$broker_port         = $kafka_base::params::broker_port,
$hostname            = $kafka_base::params::hostname,
$num_network_threads = $kafka_base::params::num_network_threads,
$num_io_threads      = $kafka_base::params::num_io_threads,
$num_partitions      = $kafka_base::params::num_partitions,
$log_retention_hours = $kafka_base::params::log_retention_hours,
$config_map          = $kafka_base::params::config_map,

$require_zookeeper   = $kafka_base::params::service_requires_zookeeper,
$service_name        = $kafka_base::params::service_name,
$service_ensure      = $kafka_base::params::service_ensure,
$service_install     = $kafka_base::params::broker_service_install,
$jmx_opts            = $kafka_base::params::broker_jmx_opts,
$heap_opts           = $kafka_base::params::broker_heap_opts,
$log4j_opts          = $kafka_base::params::broker_log4j_opts,
$opts                = $kafka_base::params::broker_opts,

$log_dirs            = $kafka_base::params::log_dirs,
$command             = $kafka_base::params::command,
$config              = $kafka_base::params::config,
$config_template     = $kafka_base::params::config_template,
$zookeeper_connect   = $kafka_base::params::zookeeper_connect,

$config_env          = $kafka_base::params::config_env,
$config_template_env = $kafka_base::params::config_template_env,

) inherits kafka_base::params {
  include kafka_base::install
  include kafka_base::config
  include kafka_base::service
}
