class kafka_base::install inherits kafka_base {

    require dir_base::src
    require dir_base::kafka

    if $hostname == 'c1' {
      $mirror = $link_kafka_vn
    }else {
      $mirror = $link_kafka_us
    }

    exec { "tar -xzf /opt/src/kafka_2.11-0.9.0.1.tgz":
      cwd     => "/opt/src",
      path    => ["/bin"],
      creates => '/opt/src/kafka_2.11-0.9.0.1',
      require => Exec['wget_package_kafka'],
    }

    exec { 'wget_package_kafka':
      command   => "wget $mirror -O /opt/src/kafka_2.11-0.9.0.1.tgz",
      path    => ["/sbin","/bin","/usr/bin", "/usr/sbin"],
      creates => '/opt/src/kafka_2.11-0.9.0.1.tgz',
    }
}
