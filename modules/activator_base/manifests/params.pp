# == Class: redis_base::params
#
class activator_base::params {

      $service_name        = 'activator'
      $service_fullname    = "typesafe-$service_name"
      $service_module      = 'activator_base'
      $version             = '1.3.12'
      $extension           = 'zip'
      $package_dir         = '/opt/src'
      $config_env          = '/etc/profile.d/activator.sh'
      $config_template_env = "$service_module/activator.sh.erb"

      # https://downloads.typesafe.com/typesafe-activator/1.3.12/typesafe-activator-1.3.12-minimal.zip
      $path_url            = "https://downloads.typesafe.com/$service_fullname"

}
