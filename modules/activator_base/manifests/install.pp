class activator_base::install inherits activator_base {

    require dir_base::src

    # https://downloads.typesafe.com/typesafe-activator/1.3.12/typesafe-activator-1.3.12-minimal.zip
    $mirror       = "$path_url/$version/$package_name.$extension"

    exec { 'wget_package_activator':
      command     => "wget $mirror -O $tmp_dir.$extension",
      path        => ["/sbin","/bin","/usr/bin", "/usr/sbin"],
      creates     => "$install_dir",
    }

    exec { 'unzip_package_activator':
      command     => "unzip $tmp_dir.$extension",
      cwd         => "/opt/src",
      path        => ["/sbin","/bin","/usr/bin", "/usr/sbin"],
      creates     => "$install_dir",
      require     => Exec['wget_package_activator'],
    }

}
