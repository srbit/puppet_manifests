class activator_base (

  $service_name        = $activator_base::params::service_name,
  $service_fullname    = $activator_base::params::service_fullname,
  $service_module      = $activator_base::params::service_module,
  $version             = $activator_base::params::version,
  $extension           = $activator_base::params::extension,
  $package_dir         = $activator_base::params::package_dir,
  $config_env          = $activator_base::params::config_env,
  $config_template_env = $activator_base::params::config_template_env,

  # https://downloads.typesafe.com/typesafe-activator/1.3.12/typesafe-activator-1.3.12-minimal.zip
  $path_url            = $activator_base::params::path_url,

  )inherits activator_base::params {

    $package_name         = "$service_fullname-$version-minimal"
    $package_name_install = "$service_name-$version-minimal"
    $install_dir          = "$package_dir/$package_name_install"
    $tmp_dir              = "$package_dir/$package_name"


    include activator_base::install
    include activator_base::config
    include activator_base::service


  }
