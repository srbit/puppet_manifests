# == Class: node_base::config
#
class activator_base::config inherits activator_base {

  file { $config_env:
    ensure  => file,
    # owner   => root,
    # group   => root,
    mode    => 0644,
    content => template($config_template_env),
  }

}
