class redis_base (

  $service_name        = $redis_base::params::service_name,
  $service_fullname    = $redis_base::params::service_fullname,
  $service_module      = $redis_base::params::service_module,
  $version             = $redis_base::params::version,
  $extension           = $redis_base::params::extension,
  $package_dir         = $redis_base::params::package_dir,
  $config_template     = $redis_base::params::config_template,
  $config_env          = $redis_base::params::config_env,
  $config_template_env = $redis_base::params::config_template_env,

  # http://download.redis.io/releases/redis-3.2.6.tar.gz
  $path_url            = $redis_base::params::path_url,

  ) inherits redis_base::params  {

    $package_name        = "$service_fullname-$version"
    $install_dir         = "$package_dir/$package_name"
    $config_dir          = "$install_dir"
    $config              = "$config_dir/redis.conf"

    include redis_base::config
    include redis_base::service


}
