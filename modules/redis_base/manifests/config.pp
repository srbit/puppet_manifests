class redis_base::config inherits redis_base {

  require dir_base::src
  require redis_base::install

  file { $config:
    ensure  => file,
    # owner   => root,
    # group   => root,
    mode    => 0644,
    content => template($config_template),
  }

  file { $config_env:
    ensure  => file,
    # owner   => root,
    # group   => root,
    mode    => 0644,
    content => template($config_template_env),
  }

  # file { $config_supervisord:
  #   ensure  => file,
  #   owner   => root,
  #   group   => root,
  #   mode    => '0644',
  #   require => Package['supervisor'],
  #   content => template($config_template_supervisor),
  # }

}
