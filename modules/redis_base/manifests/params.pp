# == Class: redis_base::params
#
class redis_base::params {

      $service_name        = 'redis'
      $service_fullname    = 'redis'
      $service_module      = 'redis_base'
      $version             = '3.2.6'
      $extension           = 'tar.gz'
      $package_dir         = '/opt/src'
      $config_template     = "$service_module/redis.conf.erb"
      $config_env          = '/etc/profile.d/redis.sh'
      $config_template_env = "$service_module/redis.sh.erb"
      $redis_port          = 6379

      # http://download.redis.io/releases/redis-3.2.6.tar.gz
      $path_url            = "http://download.redis.io/releases"

}
