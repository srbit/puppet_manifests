# == Class: redis_base::install

class redis_base::install inherits redis_base {

    $mirror       = "$path_url/$package_name.$extension"

    exec { 'wget_package_redis':
      command     => "wget $mirror -O $install_dir.$extension",
      path        => ["/sbin","/bin","/usr/bin", "/usr/sbin"],
      creates     => "$install_dir.$extension",
    }

    exec { 'tar_package_redis':
      command     => "tar -xzf $install_dir.$extension",
      cwd         => "/opt/src",
      path        => ["/bin"],
      creates     => "$install_dir",
      require     => Exec['wget_package_redis'],
    }

    exec { 'make_package_redis':
      command     => 'make',
      cwd         => "$install_dir/src",
      path        => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
      creates     => "$install_dir/src/redis-server",
      require     => [Exec['tar_package_redis'],
                      Package['gcc']],
    }

    # exec { 'test_package_redis':
    #   command     => 'make test',
    #   cwd         => "$install_dir/src",
    #   path        => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
    #   require     => [Exec['make_package_redis'],
    #                   Package['tcl']],
    # }

    file { $install_dir:
      ensure      => directory,
      recurse     => true,
      # owner     => "elasticsearch",
      # group     => "root",
      require     => Exec['tar_package_redis'],
    }

    package { 'gcc':
      ensure      => installed,
    }

    package { 'tcl':
      ensure      => installed,
    }

}
