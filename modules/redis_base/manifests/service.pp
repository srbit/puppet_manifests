# == Class: redis_base::service
#
class redis_base::service inherits redis_base {

    file { "${service_name}.service":
      ensure      => file,
      path        => "/etc/init.d/${service_name}",
      # owner       => root,
      # group       => root,
      mode        => 0755,
      content     => template('redis_base/init.erb'),
      before      => Service[$service_name],
    }

    service { $service_name:
      ensure      => $service_ensure,
      enable      => true,
      hasstatus   => true,
      hasrestart  => true,
    }

    exec { 'dir_data_redis':
      command     => "mkdir -p $install_dir/data",
      path        => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
      creates     => "$install_dir/data",
    }

}
