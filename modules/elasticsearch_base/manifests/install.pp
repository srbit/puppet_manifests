class elasticsearch_base::install inherits elasticsearch_base {

    if($version=='5.1.1'){
      $mirror = "$path_url_5x/$package_name.$extension"
    }
    if($version=='2.4.3'){
      $mirror = "$path_url_2x/$version/$package_name.$extension"
    }
    if($version=='1.7.6'){
      $mirror = "$path_url_1x/$package_name.$extension"
    }

    exec { 'tar_package_elasticsearch':
      command => "tar -xzf $install_dir.$extension",
      cwd     => "/opt/src",
      path    => ["/bin"],
      creates => "$install_dir",
      require => Exec['wget_package_elasticsearch'],
    }

    exec { 'wget_package_elasticsearch':
      command   => "wget $mirror -O $install_dir.$extension",
      path      => ["/sbin","/bin","/usr/bin", "/usr/sbin"],
      creates   => "$install_dir.$extension",
    }

    file { $install_dir:
      ensure => directory,
      recurse => true,
      # owner => "elasticsearch",
      # group => "root",
      require => Exec['tar_package_elasticsearch'],
    }

}
