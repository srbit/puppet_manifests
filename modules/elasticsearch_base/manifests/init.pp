class elasticsearch_base (

    $service_name        = $elasticsearch_base::params::service_name,
    $service_fullname    = $elasticsearch_base::params::service_fullname,
    $service_module      = $elasticsearch_base::params::service_module,
    $version             = $elasticsearch_base::params::version,
    $extension           = $elasticsearch_base::params::extension,
    $package_dir         = $elasticsearch_base::params::package_dir,
    $config_template     = $elasticsearch_base::params::config_template,

    $path_url_5x         = $elasticsearch_base::params::path_url_5x,
    $path_url_2x         = $elasticsearch_base::params::path_url_2x,
    $path_url_1x         = $elasticsearch_base::params::path_url_1x,

    $service_restart     = $elasticsearch_base::params::service_restart,

    $config_supervisord          = $elasticsearch_base::params::config_supervisord,
    $config_template_supervisor  = $elasticsearch_base::params::config_template_supervisor,

    # Cluster
    $cluster_name                = $elasticsearch_base::params::cluster_name,

    # Node
    $node_name                   = $elasticsearch_base::params::node_name,
    $node_master                 = $elasticsearch_base::params::node_master,
    $node_data                   = $elasticsearch_base::params::node_data,
    $node_rack                   = $elasticsearch_base::params::node_rack,

    # Index
    $index_number_of_shards      = $elasticsearch_base::params::index_number_of_shards,
    $index_number_of_replicas    = $elasticsearch_base::params::index_number_of_replicas,

    # Paths
    $path_conf                   = $config_dir,
    $path_data                   = $elasticsearch_base::params::path_data,
    $path_data_dir               = $elasticsearch_base::params::path_data_dir,
    $path_data_1x                = $elasticsearch_base::params::path_data_1x,
    $path_data_2x                = $elasticsearch_base::params::path_data_2x,
    $path_data_5x                = $elasticsearch_base::params::path_data_5x,
    $path_work                   = $elasticsearch_base::params::path_work,
    $path_logs                   = $elasticsearch_base::params::path_logs,
    $path_plugins                = $elasticsearch_base::params::path_plugins,

    # Plugin
    $plugin_mandatory            = $elasticsearch_base::params::plugin_mandatory,

    # Memory
    $bootstrap_mlockall          = $elasticsearch_base::params::bootstrap_mlockall, #1x
    $bootstrap_memory_lock       = $elasticsearch_base::params::bootstrap_memory_lock, #2x,5x

    # Network And HTTP
    $network_bind_host           = $elasticsearch_base::params::network_bind_host,
    $network_publish_host        = $elasticsearch_base::params::network_publish_host,
    $network_host                = $elasticsearch_base::params::network_host,
    $transport_tcp_port          = $elasticsearch_base::params::transport_tcp_port,
    $transport_tcp_compress      = $elasticsearch_base::params::transport_tcp_compress,
    $http_port                   = $elasticsearch_base::params::http_port,
    $http_max_content_length     = $elasticsearch_base::params::http_max_content_length,
    $http_enabled                = $elasticsearch_base::params::http_enabled,

    # Gateway
    $gateway_type                = $elasticsearch_base::params::gateway_type,
    $gateway_recover_after_nodes = $elasticsearch_base::params::gateway_recover_after_nodes, #Value: 1x 1, 2x 3, 5x
    $gateway_recover_after_time  = $elasticsearch_base::params::gateway_recover_after_time,
    $gateway_expected_nodes      = $elasticsearch_base::params::gateway_expected_nodes,

    # Recovery Throttling
    $cluster_routing_allocation_node_initial_primaries_recoveries = $elasticsearch_base::params::cluster_routing_allocation_node_initial_primaries_recoveries,
    $cluster_routing_allocation_node_concurrent_recoveries        = $elasticsearch_base::params::cluster_routing_allocation_node_concurrent_recoveries,
    $indices_recovery_max_bytes_per_sec                           = $elasticsearch_base::params::indices_recovery_max_bytes_per_sec,
    $indices_recovery_concurrent_streams                          = $elasticsearch_base::params::indices_recovery_concurrent_streams,

    # Discovery
    $discovery_zen_minimum_master_nodes   = $elasticsearch_base::params::discovery_zen_minimum_master_nodes, #Value: 1x 1, 2x 3, 5x
    $discovery_zen_ping_timeout           = $elasticsearch_base::params::discovery_zen_ping_timeout,
    $discovery_zen_fd_ping_timeout        = $elasticsearch_base::params::discovery_zen_fd_ping_timeout,
    $discovery_zen_ping_multicast_enabled = $elasticsearch_base::params::discovery_zen_ping_multicast_enabled,
    $discovery_zen_ping_unicast_enabled   = $elasticsearch_base::params::discovery_zen_ping_unicast_enabled,
    $discovery_zen_ping_unicast_hosts     = $elasticsearch_base::params::discovery_zen_ping_unicast_hosts,

    # Slow Log
    $index_search_slowlog_threshold_query_warn     = $elasticsearch_base::params::index_search_slowlog_threshold_query_warn,
    $index_search_slowlog_threshold_query_info     = $elasticsearch_base::params::index_search_slowlog_threshold_query_info,
    $index_search_slowlog_threshold_query_debug    = $elasticsearch_base::params::index_search_slowlog_threshold_query_debug,
    $index_search_slowlog_threshold_query_trace    = $elasticsearch_base::params::index_search_slowlog_threshold_query_trace,

    $index_search_slowlog_threshold_fetch_warn     = $elasticsearch_base::params::index_search_slowlog_threshold_fetch_warn,
    $index_search_slowlog_threshold_fetch_info     = $elasticsearch_base::params::index_search_slowlog_threshold_fetch_info,
    $index_search_slowlog_threshold_fetch_debug    = $elasticsearch_base::params::index_search_slowlog_threshold_fetch_debug,
    $index_search_slowlog_threshold_fetch_trace    = $elasticsearch_base::params::index_search_slowlog_threshold_fetch_trace,

    $index_indexing_slowlog_threshold_index_warn   = $elasticsearch_base::params::index_indexing_slowlog_threshold_index_warn,
    $index_indexing_slowlog_threshold_index_info   = $elasticsearch_base::params::index_indexing_slowlog_threshold_index_info,
    $index_indexing_slowlog_threshold_index_debug  = $elasticsearch_base::params::index_indexing_slowlog_threshold_index_debug,
    $index_indexing_slowlog_threshold_index_trace  = $elasticsearch_base::params::index_indexing_slowlog_threshold_index_trace,

    # GC Logging
    $monitor_jvm_gc_young_warn        = $elasticsearch_base::params::monitor_jvm_gc_young_warn,
    $monitor_jvm_gc_young_info        = $elasticsearch_base::params::monitor_jvm_gc_young_info,
    $monitor_jvm_gc_young_debug       = $elasticsearch_base::params::monitor_jvm_gc_young_debug,

    $monitor_jvm_gc_old_warn          = $elasticsearch_base::params::monitor_jvm_gc_old_warn,
    $monitor_jvm_gc_old_info          = $elasticsearch_base::params::monitor_jvm_gc_old_info,
    $monitor_jvm_gc_old_debug         = $elasticsearch_base::params::monitor_jvm_gc_old_debug,

    # Security
    $http_jsonp_enable                = $elasticsearch_base::params::http_jsonp_enable,

    # Various 2x
    $node_max_local_storage_nodes     = $elasticsearch_base::params::node_max_local_storage_nodes,
    $action_destructive_requires_name = $elasticsearch_base::params::action_destructive_requires_name,

  ) inherits elasticsearch_base::params {

    $package_name        = "$service_fullname-$version"
    $install_dir         = "$package_dir/$package_name"
    $config_dir          = "$install_dir/config"
    $config              = "$config_dir/elasticsearch.yml"

    include elasticsearch_base::config
    include elasticsearch_base::service

}
