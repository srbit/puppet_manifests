class elasticsearch_base::config inherits elasticsearch_base {

  require user_base::elasticsearch
  require dir_base::src
  require dir_base::elasticsearch
  require elasticsearch_base::install

  file { $config:
    ensure  => file,
    # owner   => elasticsearch,
    # group   => root,
    mode    => '0644',
    content => template($config_template),
  }

  # file { $config_supervisord:
  #   ensure  => file,
  #   owner   => root,
  #   group   => root,
  #   mode    => '0644',
  #   require => Package['supervisor'],
  #   content => template($config_template_supervisor),
  # }

}
