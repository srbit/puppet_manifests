class elasticsearch_base::params {

    $service_name        = 'elasticsearch'
    $service_fullname    = 'elasticsearch'
    $service_module      = 'elasticsearch_base'
    $version             = '1.7.6'
    $extension           = 'tar.gz'
    $package_dir         = '/opt/src'
    $config_template     = "$service_module/elasticsearch.yml.erb"

    # $mirror_url_5x = "https://artifacts.elastic.co/downloads/$service_name/$package_name.$extension"
    $path_url_5x = "https://artifacts.elastic.co/downloads/$service_name"
    # $mirror_url_2x = "https://download.elastic.co/elasticsearch/release/org/elasticsearch/distribution/tar/$service_name/$version/$package_name.$extension"
    $path_url_2x = "https://download.elastic.co/elasticsearch/release/org/elasticsearch/distribution/tar/$service_name"
    # $mirror_url_1x = "https://download.elastic.co/$service_name/$service_name/$package_name.$extension"
    $path_url_1x = "https://download.elastic.co/$service_name/$service_name"


    $service_restart     = true

    $config_supervisord          = '/etc/supervisord.conf'
    $config_template_supervisor  = "$service_module/supervisord.conf.erb"

    # Cluster
    $cluster_name                = 'fimplus-log-center'

    # Node
    $node_name                   = 'ae1-master1'
    $node_master                 = true
    $node_data                   = true
    $node_rack                   = undef

    # Index
    $index_number_of_shards      = 4
    $index_number_of_replicas    = 1

    # Paths
    $path_conf                   = $config_dir
    $path_data                   = '/mnt/data/data_elastic'
    $path_data_dir               = '/mnt/data/data_elastic'
    $path_data_1x                = "$path_data_dir/data_1x"
    $path_data_2x                = "$path_data_dir/data_2x"
    $path_data_5x                = "$path_data_dir/data_5x"
    $path_work                   = undef
    $path_logs                   = undef
    $path_plugins                = undef

    # Plugin
    $plugin_mandatory            = 'mapper-attachments,lang-groovy'

    # Memory
    $bootstrap_mlockall          = true #1x
    $bootstrap_memory_lock       = true #2x,5x

    # Network And HTTP
    $network_bind_host           = '192.168.0.1'
    $network_publish_host        = '192.168.0.1'
    $network_host                = '192.168.0.1'
    $transport_tcp_port          = 9300
    $transport_tcp_compress      = true
    $http_port                   = 9200
    $http_max_content_length     = '100mb'
    $http_enabled                = true

    # Gateway
    $gateway_type                = 'local'
    $gateway_recover_after_nodes = 1 #Value: 1x 1, 2x 3, 5x
    $gateway_recover_after_time  = '5m'
    $gateway_expected_nodes      = 2

    # Recovery Throttling
    $cluster_routing_allocation_node_initial_primaries_recoveries = 4
    $cluster_routing_allocation_node_concurrent_recoveries        = 2
    $indices_recovery_max_bytes_per_sec                           = '200mb'
    $indices_recovery_concurrent_streams                          = 5

    # Discovery
    $discovery_zen_minimum_master_nodes   = 1 #Value: 1x 1, 2x 3, 5x
    $discovery_zen_ping_timeout           = '3s'
    $discovery_zen_fd_ping_timeout        = '30s'
    $discovery_zen_ping_multicast_enabled = false
    $discovery_zen_ping_unicast_enabled   = true
    $discovery_zen_ping_unicast_hosts     = ["ae1", "ae2"]

    # Slow Log
    $index_search_slowlog_threshold_query_warn     = '10s'
    $index_search_slowlog_threshold_query_info     = '5s'
    $index_search_slowlog_threshold_query_debug    = '2s'
    $index_search_slowlog_threshold_query_trace    = '500ms'

    $index_search_slowlog_threshold_fetch_warn     = '1s'
    $index_search_slowlog_threshold_fetch_info     = '800ms'
    $index_search_slowlog_threshold_fetch_debug    = '500ms'
    $index_search_slowlog_threshold_fetch_trace    = '200ms'

    $index_indexing_slowlog_threshold_index_warn   = '10s'
    $index_indexing_slowlog_threshold_index_info   = '5s'
    $index_indexing_slowlog_threshold_index_debug  = '2s'
    $index_indexing_slowlog_threshold_index_trace  = '500ms'

    # GC Logging
    $monitor_jvm_gc_young_warn        = '1000ms'
    $monitor_jvm_gc_young_info        = '700ms'
    $monitor_jvm_gc_young_debug       = '400ms'

    $monitor_jvm_gc_old_warn          = '10s'
    $monitor_jvm_gc_old_info          = '5s'
    $monitor_jvm_gc_old_debug         = '2s'

    # Security
    $http_jsonp_enable                = true

    # Various 2x
    $node_max_local_storage_nodes     = 1
    $action_destructive_requires_name = true

}
