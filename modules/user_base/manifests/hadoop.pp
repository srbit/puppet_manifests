# == Class: user_base::hadoop
#
class user_base::hadoop inherits user_base  {

    user { 'hadoop':
      comment    => 'Hadoop User',
      home       => '/home/hadoop',
      ensure     => present,
      shell      => '/bin/bash',
      uid        => '10011',
      gid        => '10011',
      password   => 'aA123123',
      managehome => true,
    }

    group { 'hadoop':
      gid        => 10011,
    }

    exec { 'dir_hadoop_ssh':
      command    => "mkdir -p /home/hadoop/.ssh",
      path       => ["/bin"],
      creates    => "/home/hadoop/.ssh",
      require    => User['hadoop'],
    }

    file { '/home/hadoop/.ssh':
      ensure     => directory,
      mode       => 0700,
      owner      => 'hadoop',
      group      => 'hadoop',
    }

    file { "/home/hadoop/.ssh/authorized_keys":
	    owner      => 'hadoop',
	    group      => 'hadoop',
	    mode       => 0600,
	    content	   =>	template ("user_base/authorized_keys.erb"),
	    require    => [User['hadoop'],Group['hadoop'],Exec['dir_hadoop_ssh']],
		}

    file { "/home/hadoop/.ssh/id_rsa":
      owner      => 'hadoop',
      group      => 'hadoop',
      mode       => 0600,
      content	   =>	template ("user_base/id_rsa.erb"),
      require    => [User['hadoop'],Group['hadoop'],Exec['dir_hadoop_ssh']],
    }

}
