# == Class: user_base::elasticsearch
#
class user_base::elasticsearch inherits user_base  {

    user { 'elasticsearch':
      comment => 'Elasticsearch User',
      home => '/home/elasticsearch',
      ensure => present,
      shell => '/bin/bash',
      uid => '5000',
      gid => '5000',
      password => 'aA123123',
      managehome => true,
    }

    group { 'elasticsearch':
      gid => 5000,
    }

}
