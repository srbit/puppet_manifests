class bash_base::params {

      $service_module      = 'bash_base'
      $config_env          = '/etc/profile.d/cmd_alias.sh'
      $config_template_env = "$service_module/cmd_alias.sh.erb"

}
