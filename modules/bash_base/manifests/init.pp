class bash_base(

  $config_env          = $bash_base::params::config_env,
  $config_template_env = $bash_base::params::config_template_env,

  ) inherits bash_base::params{

  include bash_base::config

}
