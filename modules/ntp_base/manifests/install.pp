# == Class: ntp::install
class ntp_base::install inherits ntp_base {

  package { 'ntp':
    ensure => installed,
  }

}
