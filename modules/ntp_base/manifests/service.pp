# == Class: ntp::service
class ntp_base::service inherits ntp_base {

  service { 'ntpd':
    ensure     => running,
    enable     => true,
    hasstatus  => true,
    hasrestart => true,
    require => Package['ntp'],
  }

}
