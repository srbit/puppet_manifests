# == Class: ntp::config
class ntp_base::config inherits ntp_base {

  file { '/etc/ntp.conf':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => 0644,
    content => template('ntp_base/ntp.conf.erb'),
  }

}
