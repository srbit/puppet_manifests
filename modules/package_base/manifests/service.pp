class package_base::service inherits package_base {

  service { 'supervisord':
    ensure     => running,
    enable     => true,
    hasstatus  => true,
    hasrestart => true,
    require => Package['supervisor'],
  }

}
