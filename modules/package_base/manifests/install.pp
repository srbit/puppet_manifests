class package_base::install inherits package_base {

 package { 'epel-release-6-8':
   ensure          => installed,
   source          => 'http://download.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm'
 }

  package { 'telnet':
    ensure => 'installed',
  }

  package { 'supervisor':
    ensure => installed,
  }

  package { 'git':
    ensure => installed,
  }

}
