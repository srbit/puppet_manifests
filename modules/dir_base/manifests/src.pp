# == Class: dirbase::src
class dir_base::src inherits dir_base {

  exec {'mkdir -p /opt/src':
  cwd     => "/opt",
  path    => ["/bin"],
  creates => '/opt/src'
  }

}
