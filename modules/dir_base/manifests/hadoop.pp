# == Class: dir_base::hadoop
#
class dir_base::hadoop inherits dir_base {

  require user_base::hadoop

  exec { 'dir_namenode':
    command => 'mkdir -p /mnt/data/hadoop/namenode',
    cwd     => "/usr",
    path    => ["/bin"],
    creates => '/mnt/data/hadoop/namenode'
  }

  file { '/mnt/data/hadoop/namenode' :
    ensure  => directory,
    owner   => 'hadoop',
    group   => 'hadoop',
    require => Exec['dir_namenode'],
  }

  exec { 'dir_datanode':
    command => 'mkdir -p /mnt/data/hadoop/datanode',
    cwd     => "/usr",
    path    => ["/bin"],
    creates => '/mnt/data/hadoop/datanode'
  }

  file { '/mnt/data/hadoop/datanode' :
    ensure  => directory,
    owner   => 'hadoop',
    group   => 'hadoop',
    require => Exec['dir_datanode'],
  }

  exec { 'dir_tmp':
    command => 'mkdir -p /mnt/data/hadoop/tmp',
    cwd     => "/usr",
    path    => ["/bin"],
    creates => '/mnt/data/hadoop/tmp'
  }

  file { '/mnt/data/hadoop/tmp' :
    ensure  => directory,
    owner   => 'hadoop',
    group   => 'hadoop',
    require => Exec['dir_tmp'],
  }

  exec { 'dir_journal':
    command => 'mkdir -p /opt/src/hadoop-2.7.2/data/jn',
    cwd     => "/usr",
    path    => ["/bin"],
    creates => '/opt/src/hadoop-2.7.2/data/jn',
    require => Exec['tar_package_hadoop'],
  }

  file { '/opt/src/hadoop-2.7.2/data/jn' :
    ensure  => directory,
    owner   => 'hadoop',
    group   => 'hadoop',
    require => [ User['hadoop'], Group['hadoop'], Exec['dir_journal']],
  }

}
