class dir_base {

  include dir_base::java
  include dir_base::src
  include dir_base::ssh_key

  if $hostname != 'c1' {
    include dir_base::host
    include dir_base::ssh_key
  }

}
