# == Class: dirbase::src
class dir_base::kafka inherits dir_base {

  exec {'mkdir -p /mnt/resource/kafka-logs':
  cwd     => "/usr",
  path    => ["/bin"],
  creates => '/mnt/resource/kafka-logs'
  }

}
