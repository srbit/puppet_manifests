class dir_base::ssh_key inherits dir_base {

ssh_authorized_key { 'jenkins@aa1':
  user => 'root',
  type => 'ssh-rsa',
  key  => 'AAAAB3NzaC1yc2EAAAABIwAAAQEA5syEUAtSqF4qBJ97qwJP2z4lB0cP3NITBjv+jKCbRlCoHddeBHS84JrPSmbKhf2jabpk59SMn42NDBhp7zkQaAYkdhg62dlv/OSzDQSU9CfUmM7sZVLcjyDcbj5rrK0G5apBgNPXbv3F28iRKDEyqaTB4NYjEOO2yPKnngNIJBCgVsAyK2yynQA0/x98+6jFTPgfibHRrNvN5MIRvE95gj+zNV6A+NW5rHa9E3BUTi7oJSGfjymfbqAHPxh4RbniNy4ON/6Wv9nepLpSwV/9CKckS5qBzbzkMlv/ExiT335gECDIyFoW3OETD/hHmZi4BDwm5kXR1gtDC8e/80N6Jw==',
}

# file { '~/.ssh/id_rsa':
#   ensure  => file,
#   mode    => 0700,
#   content => template('dir_base/id_rsa.erb'),
# }

}
