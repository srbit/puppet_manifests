# == Class: dirbase::java
class dir_base::java inherits dir_base {

  exec {'mkdir -p /usr/java':
  cwd     => "/usr",
  path    => ["/bin"],
  creates => '/usr/java'
  }

}
