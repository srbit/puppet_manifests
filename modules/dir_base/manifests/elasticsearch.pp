class dir_base::elasticsearch inherits dir_base {

  exec { 'data_1x':
    command => 'mkdir -p /mnt/data/data_elastic/data_1x',
    cwd     => "/usr",
    path    => ["/bin"],
    creates => '/mnt/data/data_elastic/data_1x'
  }

  exec { 'data_2x':
    command => 'mkdir -p /mnt/data/data_elastic/data_2x',
    cwd     => "/usr",
    path    => ["/bin"],
    creates => '/mnt/data/data_elastic/data_2x'
  }

  exec { 'data_5x':
    command => 'mkdir -p /mnt/data/data_elastic/data_5x',
    cwd     => "/usr",
    path    => ["/bin"],
    creates => '/mnt/data/data_elastic/data_5x'
  }

  file { "/mnt/data/data_elastic/data_1x":
    ensure  => directory,
    # recurse => true,
    owner   => "elasticsearch",
    group   => "elasticsearch",
    mode    => 0644,
    require => Exec['data_1x'],
  }

  file { "/mnt/data/data_elastic/data_2x":
    ensure  => directory,
    # recurse => true,
    owner   => "elasticsearch",
    group   => "elasticsearch",
    mode    => 0644,
    require => Exec['data_2x'],
  }

  file { "/mnt/data/data_elastic/data_5x":
    ensure  => directory,
    # recurse => true,
    owner   => "elasticsearch",
    group   => "elasticsearch",
    mode    => 0644,
    require => Exec['data_5x'],
  }

}
