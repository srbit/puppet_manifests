# == Class: dirbase::zookeeper
class dir_base::zookeeper inherits dir_base {

  exec {'mkdir -p /opt/src/logs_zookeeper/logs':
  cwd     => "/usr",
  path    => ["/bin"],
  creates => '/opt/src/logs_zookeeper/logs'
  }

  exec {'mkdir -p /opt/src/logs_zookeeper/zookeeper':
  cwd     => "/usr",
  path    => ["/bin"],
  creates => '/opt/src/logs_zookeeper/zookeeper'
  }

}
