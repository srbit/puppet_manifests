# == Class: ntp::config
class dir_base::host inherits dir_base {

file { '/etc/hosts':
  ensure  => file,
  owner   => root,
  group   => root,
  mode    => '0644',
  content => template('dir_base/hosts.erb'),
}

}
