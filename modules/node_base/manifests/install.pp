class node_base::install inherits node_base {

    require dir_base::src

    $mirror       = "$path_url/v$version/$package_name.$extension"

    exec { 'wget_package_node':
      command     => "wget $mirror -O $install_dir.$extension",
      path        => ["/sbin","/bin","/usr/bin", "/usr/sbin"],
      creates     => "$install_dir",
    }

    exec { 'unxz_package_node':
      command     => "unxz $install_dir.$extension",
      cwd         => "/opt/src",
      path        => ["/sbin","/bin","/usr/bin", "/usr/sbin"],
      creates     => "$install_dir.tar",
      require     => [Exec['wget_package_node'],
                      Package['xz']],
    }

    exec { 'tar_package_node':
      command     => "tar -xvf $install_dir.tar",
      cwd         => "/opt/src",
      path        => ["/sbin","/bin","/usr/bin", "/usr/sbin"],
      creates     => "$install_dir",
      require     => Exec['unxz_package_node'],
    }

    package { 'xz':
      ensure      => installed,
    }

    exec { 'install_package_pm2':
      command     => "npm install pm2@latest -g",
      cwd         => "/opt/src",
      path        => ["$install_dir/bin"],
      creates     => "$install_dir/bin/pm2",
      require     => Exec['tar_package_node'],
    }

}
