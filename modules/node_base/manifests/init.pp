class node_base (

  $service_name        = $node_base::params::service_name,
  $service_fullname    = $node_base::params::service_fullname,
  $service_module      = $node_base::params::service_module,
  $version             = $node_base::params::version,
  $os                  = $node_base::params::os,
  $arch_x64            = $node_base::params::arch_x64,
  $extension           = $node_base::params::extension,
  $package_dir         = $node_base::params::package_dir,
  $config_env          = $node_base::params::config_env,
  $config_template_env = $node_base::params::config_template_env,

  # https://nodejs.org/dist/v6.9.2/node-v6.9.2-linux-x64.tar.xz
  $path_url            = $node_base::params::path_url,

  ) inherits node_base::params {

    $package_name        = "$service_fullname-v$version-$os-$arch_x64"
    $install_dir         = "$package_dir/$package_name"

    include node_base::install
    include node_base::config
    include node_base::service

}
