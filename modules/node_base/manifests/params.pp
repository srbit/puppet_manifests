# == Class: redis_base::params
#
class node_base::params {

      $service_name        = 'node'
      $service_fullname    = 'node'
      $service_module      = 'node_base'
      $version             = '6.9.2'
      $os                  = 'linux'
      $arch_x64            = 'x64'
      $extension           = 'tar.xz'
      $package_dir         = '/opt/src'
      $config_env          = '/etc/profile.d/node.sh'
      $config_template_env = "$service_module/node.sh.erb"

      # https://nodejs.org/dist/v6.9.2/node-v6.9.2-linux-x64.tar.xz
      $path_url            = "https://nodejs.org/dist"

}
