import "src/base/base.pp"
import "src/kafka/init.pp"
import "src/zookeeper/init.pp"
import "src/storm/init.pp"
import "src/elastic_kibana/init.pp"
import "src/hadoop/init.pp"
# node ar1: node_base, redis_base
import "src/redis/init.pp"
# node aa1: node_base, activator_base
import "src/node/init.pp"

import "src/test/init.pp"
