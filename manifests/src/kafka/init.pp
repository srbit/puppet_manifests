node 'ak1','ak2', 'lk1', 'lk2' inherits default {

    if $hostname == 'ak1' {
      class { 'kafka_base':
            broker_id => 0
      }
    }elsif $hostname == 'ak2' {
      class { 'kafka_base':
            broker_id => 2
      }
    }

    if $hostname == 'lk1' {
      class { 'kafka_base':
            broker_id => 0,
            zookeeper_connect => 'lz1:2181,lz2:2181,lz3:2181',

      }
    }elsif $hostname == 'lk2' {
      class { 'kafka_base':
            broker_id => 2,
            zookeeper_connect => 'lz1:2181,lz2:2181,lz3:2181',

      }
    }

}
