node default {
}

node 'pk1','pk2' { include 'dir','src_hd','src_flume','tar_hd','tar_flume','cf_flume' }

class dir{

   exec {'mkdir -p /opt/src':
   cwd     => "/opt",
   path    => ["/bin"]
   }

}

class src_hd {
    require dir
    file { '/opt/src/hadoop-2.7.2.tar.gz':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => 644,
    source => 'puppet://pp.openstacklocal/extra_files/hadoop-2.7.2.tar.gz',
    }
}

class src_flume{
    require dir
    file { '/opt/src/apache-flume-1.6.0-bin.tar.gz':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => 644,
    source => 'puppet://pp.openstacklocal/extra_files/flume/apache-flume-1.6.0-bin.tar.gz',
    }
}

class tar_hd{
   require src_hd
   exec { "tar -xzf /opt/src/hadoop-2.7.2.tar.gz":
   cwd     => "/opt/src",
   path    => ["/bin"]
   }
}

class tar_flume{
   require src_flume
   exec { "tar -xzf /opt/src/apache-flume-1.6.0-bin.tar.gz":
   cwd     => "/opt/src",
   path    => ["/bin"]
   }
}

class cf_flume{
    require tar_flume
    file { '/opt/src/apache-flume-1.6.0-bin/conf/pk1.properties.template':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => 644,
    source => 'puppet://pp.openstacklocal/extra_files/flume/pk1.properties.template',
    }

    file { '/opt/src/apache-flume-1.6.0-bin/conf/flume-env.sh':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => 644,
    source => 'puppet://pp.openstacklocal/extra_files/flume/flume-env.sh',
    }

}




