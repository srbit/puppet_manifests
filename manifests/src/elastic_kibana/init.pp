node 'ae1','ae2','lj1','lj2' inherits default {

    if $hostname == 'ae1' {
      class { 'elasticsearch_base':
        version => '1.7.6'
      }

      class { 'kibana_base':
        version => '4.1.11'
      }

      class { 'node_base':
        version => '6.9.2'
      }

    }elsif $hostname == 'ae2' {
      class { 'elasticsearch_base':
        version => '1.7.6'
      }

      class { 'kibana_base':
        version => '4.1.11'
      }

    }elsif $hostname =='lj1' {
      class { 'elasticsearch_base':
        # version => '1.7.6'
        version      => '2.4.4'
      }

      class { 'kibana_base':
        # version => '4.1.11'
        version => '4.6.4'
      }

    } elsif $hostname =='lj2' {
      class { 'elasticsearch_base':
        # version => '1.7.6'
        version      => '2.4.4'
      }

      class { 'kibana_base':
        # version => '4.1.11'
        version => '4.6.4'
      }

    }

}
