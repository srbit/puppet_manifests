node 'aa1' inherits default {

    if $hostname == 'aa1' {
      class { 'node_base':
        version  => '6.9.2'
      }

      class { 'activator_base':
        version  => '1.3.12'
      }

    }

}
