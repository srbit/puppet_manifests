node default {
}

#if $hostname == 'lk1' {include 'dir','cf_kk1'}
#elsif $hostname == 'lk2' {include 'dir','cf_kk2'}

#node 'ln1','ln2','ld1','ld2' { include 'rpm_epel' }
#node 'ln1','ln2','ld1','ld2' { include 'package_supervisor' }
#node 'ln1','ln2','ld1','ld2' { include 'enable_supervisord' }

class rpm_epel {
   exec { "rpm -ivh http://download.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm":
   cwd     => "/opt/src",
   path    => ["/bin"]
   }
}

class package_supervisor {
  package { 'supervisor':
    ensure => 'latest',
  }
}

class enable_supervisord {
  service { 'supervisord':
    enable => true,
  }

}