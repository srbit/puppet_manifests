node default {
}

class dir {
exec {'mkdir -p /opt/src':
   cwd     => "/opt",
   path    => ["/bin"]
   }

exec {'mkdir -p /mnt/data/logs-z/zookeeper':
   cwd     => "/mnt",
   path    => ["/bin"]
   }

}

class src_zk {
  require dir

    file { '/opt/src/zookeeper-3.4.8.tar.gz':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => 644,
    source => 'puppet://pp.openstacklocal/extra_files/zookeeper-3.4.8.tar.gz',
    }
}

class tar_zk {
  require src_zk

   exec { "tar -xzf /opt/src/zookeeper-3.4.8.tar.gz":
   cwd     => "/opt/src",
   path    => ["/bin"]
   }
}

class env_zk{

    file { '~/.bashrc':
    ensure => present,
    }

    file_line { 'Append a line to ~/.bashrc':
    path => '~/.bashrc',
    line => 'export ZOOKEEPER_HOME=/opt/src/zookeeper-3.4.8',
}
    file_line { 'Append a line to ~/.bashrc':
    path => '~/.bashrc',
    line => 'PATH=$PATH:$ZOOKEEPER_HOME/bin',
    }
}


class zk{
    exec { 'zookeeper':
    command   => 'wget http://mirrors.viethosting.vn/apache/zookeeper/stable/zookeeper-3.4.8.tar.gz -O /opt/src/zookeeper-3.4.8.tar.gz',
    path    => ["/sbin","/bin","/usr/bin", "/usr/sbin"],
    }
}

class cf_zk{
    file { '/opt/src/zookeeper-3.4.8/conf/zoo.cfg':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => 644,
    source => 'puppet://pp.openstacklocal/extra_files/zoo.cfg.prod',
    }
}

class cf_zk1 inherits cf_zk{
   file { '/mnt/data/logs-z/zookeeper/myid':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => 644,
    content => '1',
    }
}
class cf_zk2 inherits cf_zk{
   file { '/mnt/data/logs-z/zookeeper/myid':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => 644,
    content => '2',
    }
}
class cf_zk3 inherits cf_zk{
   file { '/mnt/data/logs-z/zookeeper/myid':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => 644,
    content => '3',
    }
}
