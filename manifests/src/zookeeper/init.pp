node 'az1','az2','az3','lz1','lz2','lz3' inherits default {
# PROD
    if $hostname == 'az1' {
      class { 'zookeeper_base':
            myid => 1
      }
    }

    if $hostname == 'az2' {
      class { 'zookeeper_base':
            myid => 2
      }
    }

    if $hostname == 'az3' {
      class { 'zookeeper_base':
            myid => 3
      }
    }

# DEV & STAGING
    if $hostname == 'lz1' {
      class { 'zookeeper_base':
            myid => 1,
            prefix => 'l',
      }
    }

    if $hostname == 'lz2' {
      class { 'zookeeper_base':
            myid => 2,
            prefix => 'l',
      }
    }

    if $hostname == 'lz3' {
      class { 'zookeeper_base':
            myid => 3,
            prefix => 'l',
      }
    }

}
