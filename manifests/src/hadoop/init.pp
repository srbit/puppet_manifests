node 'an1','an2','ad1','ad2' inherits default {

    if $hostname == 'an1' {
        class { 'hadoop_base':
          version => '2.7.2'
        }
    }elsif $hostname == 'an2' {
        class { 'hadoop_base':
          version => '2.7.2'
        }
    }elsif $hostname == 'ad1' {
        class { 'hadoop_base':
          version        => '2.7.2',
          daemon_default => 'datanode',
        }
    }elsif $hostname == 'ad2' {
        class { 'hadoop_base':
          version        => '2.7.2',
          daemon_default => 'datanode',
        }
    }

}
