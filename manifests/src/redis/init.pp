node 'ar1' inherits default {

    if $hostname == 'ar1' {
      class { 'redis_base':
        version => '3.2.6'
      }

      class { 'node_base':
        version => '6.9.2'
      }

    }

}
