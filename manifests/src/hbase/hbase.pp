node default {
}

#node 'lj1','lj2' { include 'dir','package','timezone' }
#node 'lj1','lj2' { include 'src_hbase','src_jdk' }
#node 'lj1','lj2' { include 'tar_hbase','tar_jdk' }

#node 'lj1','lj2' { include 'src_hbase' }
#node 'lj1','lj2' { include 'tar_hbase' }
#node 'lj1','lj2','ld1','ld2' { include 'src_hbase' }
#node 'lj1','lj2','ld1','ld2' { include 'tar_hbase' }


#node 'lj1','lj2' { include 'src_hbase','src_jdk' }
#node 'lj1','lj2' { include 'cmd_jdk' }

#if $hostname == 'lk1' {include 'dir','cf_kk1'}
#elsif $hostname == 'lk2' {include 'dir','cf_kk2'}

class src_hbase {
    file { '/opt/src/hbase-1.1.4-bin.tar.gz':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => 644,
    source => 'puppet://pp.openstacklocal/extra_files/hbase-1.1.4-bin.tar.gz',
    }
}

class tar_hbase {
   exec { "tar -xzf /opt/src/hbase-1.1.4-bin.tar.gz":
   cwd     => "/opt/src",
   path    => ["/bin"]
   }
}