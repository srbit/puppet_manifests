node default {
}

#node 'lj1','lj2' { include 'dir','package','timezone' }
#node 'lj1','lj2' { include 'src_spark','src_jdk' }
#node 'lj1','lj2' { include 'tar_spark','tar_jdk' }

#node 'lj1','lj2' { include 'src_spark' }
#node 'lj1','lj2' { include 'tar_spark' }
#node 'lj1','lj2','ld1','ld2' { include 'src_spark' }
#node 'lj1','lj2','ld1','ld2' { include 'tar_spark' }


#node 'lj1','lj2' { include 'src_spark','src_jdk' }
#node 'lj1','lj2' { include 'cmd_jdk' }

#if $hostname == 'lk1' {include 'dir','cf_kk1'}
#elsif $hostname == 'lk2' {include 'dir','cf_kk2'}

class dir{	
exec {'mkdir -p /opt/src':
   cwd     => "/opt",
   path    => ["/bin"]
   }

   exec {'mkdir -p /usr/java':
   cwd     => "/usr",
   path    => ["/bin"]
   }
}

class package {
  package { 'wget':
    ensure => 'latest',
  }

  package { 'ntp':
    ensure => 'latest',
  }
  service { 'ntpd':
    name   => 'ntpd',
    ensure => running,
  }

  package { 'telnet':
    ensure => 'latest',
  }

  package { 'sysstat':
    ensure => 'latest',
  }

  package { 'iotop':
    ensure => 'latest',
  }

  package { 'git':
    ensure => 'latest',
  }
}

class { 'timezone':
    timezone => 'Asia/Ho_Chi_Minh',
}

class src_spark{
    file { '/opt/src/spark-1.6.1-bin-hadoop2.6.tgz':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => 644,
    source => 'puppet://pp.openstacklocal/extra_files/spark-1.6.1-bin-hadoop2.6.tgz',
    }
}

class src_jdk{
    file { '/usr/java/jdk-7u79-linux-x64.tar.gz':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => 644,
    source => 'puppet://pp.openstacklocal/extra_files/jdk-7u79-linux-x64.tar.gz',
    }
}

class tar_spark{
   exec { "tar -xzf /opt/src/spark-1.6.1-bin-hadoop2.6.tgz":
   cwd     => "/opt/src",
   path    => ["/bin"]
   }
}

class tar_jdk{
   exec { "tar -xzf /usr/java/jdk-7u79-linux-x64.tar.gz":
   cwd     => "/usr/java",
   path    => ["/bin"]
   }
}

class cmd_jdk{
   exec { "alternatives --install /usr/bin/java java /usr/java/jdk1.7.0_79/bin/java 2":
   cwd     => "/usr/java",
   path    => ["/usr/sbin"]
   }
   exec { "alternatives --install /usr/bin/javac javac /usr/java/jdk1.7.0_79/bin/javac 2":
   cwd     => "/usr/java",
   path    => ["/usr/sbin"]
   }
   exec { "alternatives --install /usr/bin/jar jar /usr/java/jdk1.7.0_79/bin/jar 2":
   cwd     => "/usr/java",
   path    => ["/usr/sbin"]
   }
}