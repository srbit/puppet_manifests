node 'c1' inherits default {

  if $hostname == 'c1' {

    include jdk_base

    class { 'kafka_base':
      broker_id    => 4
    }

    class { 'zookeeper_base':
      myid         => 1
    }

    class { 'storm_base':
      service_name => "nimbus"
    }
    # include storm_base

    class { 'elasticsearch_base':
      #version     => '5.1.1'
      version      => '2.4.3'
      #version     => '1.7.6'
    }

    class { 'kibana_base':
      #version     => '5.1.1'
      #version     => '4.6.3'
      version      => '4.1.11'
    }

    class { 'hadoop_base':
      #version => '2.7.3'
      version        => '2.7.2',
      daemon_default => 'datanode'
    }

    class { 'redis_base':
      version      => '3.2.6'
    }

    class { 'node_base':
      version      => '6.9.2'
    }

    class { 'activator_base':
      version      => '1.3.12'
    }

  }

}
