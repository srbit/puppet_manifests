class { 'timezone':
    timezone => 'Asia/Ho_Chi_Minh',
}
class { 'ntp':
 server_list => [ '1.asia.pool.ntp.org', '2.asia.pool.ntp.org', '3.asia.pool.ntp.org', '4.asia.pool.ntp.org' ]
}
