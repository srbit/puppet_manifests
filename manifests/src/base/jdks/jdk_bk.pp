
class src_jdk {
    file { '/usr/java/jdk-7u79-linux-x64.tar.gz':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => 644,
    source => 'puppet://pp.openstacklocal/extra_files/jdk-7u79-linux-x64.tar.gz',
    }
}

class tar_jdk {
  require src_jdk

   exec { "tar -xzf /usr/java/jdk-7u79-linux-x64.tar.gz":
   cwd     => "/usr/java",
   path    => ["/bin"]
   }
}

class cmd_jdk{
  require tar_jdk

   exec { "alternatives --install /usr/bin/java java /usr/java/jdk1.7.0_79/bin/java 2":
   cwd     => "/usr/java",
   path    => ["/usr/sbin"]
   }
   exec { "alternatives --install /usr/bin/javac javac /usr/java/jdk1.7.0_79/bin/javac 2":
   cwd     => "/usr/java",
   path    => ["/usr/sbin"]
   }
   exec { "alternatives --install /usr/bin/jar jar /usr/java/jdk1.7.0_79/bin/jar 2":
   cwd     => "/usr/java",
   path    => ["/usr/sbin"]
   }
}
