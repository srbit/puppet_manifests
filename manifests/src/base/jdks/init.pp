class { 'jdk_oracle':
  version       => 8,
  jce           => true,
  install_dir   => '/usr/java',
  default_java  => true
}
