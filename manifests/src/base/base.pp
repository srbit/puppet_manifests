import "times/init.pp"
# import "jdks/init.pp"

node default {
    include dir_base
    include package_base
    include bash_base
    include jdk_base
}
